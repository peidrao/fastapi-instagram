from fastapi import APIRouter, HTTPException, status, Depends
from typing import List
from sqlalchemy.orm.session import Session
from db.database import get_db
from db.db_comment import create, list_all
from auth.oauth2 import get_current_user

from schemas.schemas_comment import CommentBase
from schemas.schemas_user import UserAuth

router = APIRouter(prefix='/comments', tags=['comments'])


@router.get('/comments/{post_id}',)
async def comments(post_id: int, db: Session = Depends(get_db)):
    return list_all(db, post_id)


@router.post('/create/')
async def create_comment(request: CommentBase, db: Session = Depends(get_db), current_user: UserAuth = Depends(get_current_user)):
    return create(db, request)
