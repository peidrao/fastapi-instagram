from datetime import datetime
from sqlalchemy.orm import Session
from .models import Comment
from schemas.schemas_comment import CommentBase


def create(db: Session, request: CommentBase):
    new_comment = Comment(
        text=request.text,
        username=request.username,
        post_id=request.post_id,
        timestamp=datetime.now()
    )

    db.add(new_comment)
    db.commit()
    db.refresh(new_comment)

    return new_comment


def list_all(db: Session, post_id: int):
    return db.query(Comment).filter(Comment.post_id == post_id).all()
