from fastapi import FastAPI
from db.models import Base
from db.database import engine
from routers import routers_user, routers_post, routers_comment
from fastapi.staticfiles import StaticFiles
from fastapi.middleware.cors import CORSMiddleware
from auth import authentication

app = FastAPI()


app.include_router(routers_user.router)
app.include_router(routers_post.router)
app.include_router(routers_comment.router)
app.include_router(authentication.router)


@app.get('/')
async def root():
    return 'Hello World!'


origins = [
    'http://localhost:3000'
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=['*'],
    allow_headers=['*'],
)


Base.metadata.create_all(engine)

app.mount('/images', StaticFiles(directory='images'), name='images')



