import random
import string
import shutil

from fastapi import APIRouter, HTTPException, status, UploadFile, File
from typing import List
from fastapi.param_functions import Depends
from sqlalchemy.orm.session import Session
from auth.oauth2 import get_current_user
from db.database import get_db
from db.db_post import create, list_all, destroy_post

from schemas.schemas_post import PostBase, PostDisplay
from schemas.schemas_user import UserAuth

router = APIRouter(prefix='/post', tags=['post'])

image_url_types = ['absolute', 'relative']


@router.post('/create/', response_model=PostDisplay)
async def create_post(request: PostBase, db: Session = Depends(get_db), current_user: UserAuth = Depends(get_current_user)):
    if not request.image_url_type in image_url_types:
        raise HTTPException(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
                            detail="Parameter image_url_type can only take values 'absolute' or 'relative'.")
    return create(db, request)


@router.get('/list/', response_model=List[PostDisplay])
async def get_all(db: Session = Depends(get_db)):
    return list_all(db)


@router.post('/upload/')
def upload_image(image: UploadFile = File(...), current_user: UserAuth = Depends(get_current_user)):
    letters = string.ascii_letters
    rand_str = ''.join(random.choice(letters) for i in range(6))
    new = f'_{rand_str}.'
    filename = image.filename.split('.')[1]
    filename = new + filename
    path = f'images/{filename}'
    with open(path, 'wb') as buffer:
        shutil.copyfileobj(image.file, buffer)

    return {'filename': path}


@router.get('/delete/{id}')
def destroy(id: int, db: Session = Depends(get_db), current_user: UserAuth = Depends(get_current_user)):
    return destroy_post(db, id, current_user.id)
