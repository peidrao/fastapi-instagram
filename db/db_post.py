from datetime import datetime
from fastapi import HTTPException, status
from sqlalchemy.orm.session import Session

from .models import Post
from schemas.schemas_post import PostBase


def create(db: Session, request: PostBase):
    new_post = Post(
        image_url=request.image_url,
        image_url_type=request.image_url_type,
        timestamp=datetime.now(),
        caption=request.caption,
        user_id=request.user_id
    )

    db.add(new_post)
    db.commit()
    db.refresh(new_post)

    return new_post


def list_all(db: Session):
    return db.query(Post).all()


def destroy_post(db: Session, id: int, user_id: int):
    post = db.query(Post).filter(Post.id == id).first()
    if not post:
        raise HTTPException(detail='Post not found',
                            status_code=status.HTTP_404_NOT_FOUND)

    if post.user_id != user_id:
        raise HTTPException(detail='Only post creator can delete post',
                            status_code=status.HTTP_403_FORBIDDEN)

    db.delete(post)
    db.commit()
    return 'Ok'
