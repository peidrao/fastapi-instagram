from typing import List
from fastapi import APIRouter
from fastapi.param_functions import Depends
from sqlalchemy.orm.session import Session
from db.database import get_db
from db.db_user import (
    create_user as create,
    get_all,
    get_user_by_username as get_username
)

from schemas.schemas_user import UserBase, UserDisplay


router = APIRouter(prefix='/user', tags=['user'])


@router.post('/create/', response_model=UserDisplay)
async def create_user(request: UserBase, db: Session = Depends(get_db)):
    return create(db, request)


@router.get('/', response_model=List[UserDisplay])
async def get_all_users(db: Session = Depends(get_db)):
    return get_all(db)


@router.get('/{username}', response_model=UserDisplay)
async def get_user_by_username(username: str, db: Session = Depends(get_db)):
    return get_username(db, username)
