from datetime import datetime
from typing import List
from pydantic import BaseModel

from .schemas_comment import Comment


class PostBase(BaseModel):
    image_url: str
    image_url_type: str
    caption: str
    user_id: int


class User(BaseModel):
    username: str

    class Config:
        orm_mode = True


class PostDisplay(BaseModel):
    id: int
    image_url: str
    image_url_type: str
    caption: str
    timestamp: datetime
    user: User
    comments: List[Comment]

    class Config:
        orm_mode = True
